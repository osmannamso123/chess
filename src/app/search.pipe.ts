import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  transform(posts, value) {
    return posts.filter(post => {
      return post.title.includes(value);
    });
  }
}
