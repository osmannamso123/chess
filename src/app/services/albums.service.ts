import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class AlbumsService {
  constructor(private http: HttpClient) {}
  public getAlbums() {
    return this.http.get('https://jsonplaceholder.typicode.com/photos')
      .pipe(
        map(res => res)
      );
  }
}
