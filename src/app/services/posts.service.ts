import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class PostsService {
  constructor(private http: HttpClient) {}
  public getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        map(res => res)
      );
  }
}
