import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AlbumsComponent} from './pages/albums/albums.component';
import {UsersComponent} from './pages/users/users.component';
import {PostsComponent} from './pages/posts/posts.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {PostsModule} from './pages/posts/posts.module';
import {FormsModule} from '@angular/forms';
import {SearchPipe} from './search.pipe';
import {AlbumsModule} from './pages/albums/albums.module';

const routes = [
  {path: 'albums/:page', component: AlbumsComponent},
  {path: 'posts', component: PostsComponent},
  {path: 'users', component: UsersComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    UsersComponent,
    AlbumsComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    PostsModule,
    FormsModule,
    AlbumsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
