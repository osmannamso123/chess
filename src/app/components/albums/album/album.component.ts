import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html'
})

export class AlbumComponent {
  @Input() title: string;
  @Input() url: string;
}
