import {NgModule} from '@angular/core';
import {PostComponent} from '../../components/posts/post/post.component';
import {PostsComponent} from './posts.component';

@NgModule({
  declarations: [
    PostComponent
  ],
  exports: [
    PostComponent
  ],
  imports: [],
  providers: [],
  bootstrap: [PostsComponent]
})

export class PostsModule {}
