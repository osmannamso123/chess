import {NgModule} from '@angular/core';
import {AlbumsComponent} from './albums.component';
import {AlbumComponent} from '../../components/albums/album/album.component';

@NgModule({
  declarations: [
    AlbumComponent
  ],
  exports: [
    AlbumComponent
  ],
  imports: [],
  providers: [],
  bootstrap: [AlbumsComponent]
})

export class AlbumsModule {}
