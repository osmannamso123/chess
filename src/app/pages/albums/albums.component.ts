import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlbumsService} from '../../services/albums.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  providers: [AlbumsService]
})

export class AlbumsComponent {
  albums: any = [];
  searchStr = '';
  start: number;
  end: number;
  next: number;

  constructor(private albumsService: AlbumsService, private route: ActivatedRoute) {
    route.params.subscribe(val => {
      const page = +this.route.snapshot.paramMap.get('page');
      const jump = 12;
      this.start = (page - 1) * jump;
      this.end = this.start + jump;
      this.next = page + 1;
      this.albumsService.getAlbums().subscribe(albums => {
        this.albums = albums;
      });
    });
  }
}
